//
//  Network.swift
//  NYCSchool
//
//  Created by Rémy Virin on 20/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

import Foundation


struct Network {
    
    enum NewYorkAPI: String {
        case schoolList = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        case schoolSAT = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }
    
    func executeRequest(api: NewYorkAPI, callback: @escaping (_ JSON: Any?, _ error: Error?) -> ()) {
        guard let apiUrl = URL(string: api.rawValue) else {
            assertionFailure("Can not allocate URL from \(api.rawValue)")
            return
        }
        
        let task = URLSession.shared.dataTask(with: URLRequest(url: apiUrl)) { (data, response, error) in
            guard let data = data else { return }
            do {
                let json = try JSONSerialization.jsonObject(with: data)
                DispatchQueue.main.async {
                    callback(json, nil)
                }
            } catch let error {
            DispatchQueue.main.async {
                    callback(nil, error)
                }
            }
        }
        task.resume()
    }
}
