//
//  ListSchoolCoordinator.swift
//  NYCSchool
//
//  Created by Rémy Virin on 19/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

import Foundation
import UIKit

protocol ListSchoolViewControllable: ViewControllable {
    // For compilation reasons the '!' has to be defined as well in the protocol.
    // Without a storyboard, the viewModel would be injected at the init.
    var viewModel: ListSchoolViewModelable! { get set }
    var listener: ListSchoolCoordinatable? { get set }
    func displaySchoolList()
}

final class ListSchoolCoordinator: ListSchoolCoordinatable {
    
    var coordinators = [Coordinatable]()
    
    let controller: ListSchoolViewControllable
    let navigationController: UINavigationController
    
    // Keeping an instance on the DetailViewModel, allows us to not perform another
    // api call in case the user goes back and forth the detail view.
    var schoolDetailViewModel: SchoolDetailViewModelable?
    
    init(controller: ListSchoolViewControllable, navigationController: UINavigationController) {
        controller.viewModel = ListSchoolViewModel()
        self.navigationController = navigationController
        self.controller = controller
        self.controller.listener = self
    }
    
    func start() {
        controller.displaySchoolList()
    }
    
    func finish() {
        controller.dismiss(animated: true)
        coordinators.forEach { $0.finish() }
   }
    
    // MARK: - ListSchoolCoordinatable
    
    func showDetailFor(school: Schoolable) {
        
        let storyboard = UIStoryboard(name: UIConstants.mainStoryboard, bundle: nil)
        if let detailController = storyboard.instantiateViewController(withIdentifier: UIConstants.schoolDetailViewController) as? SchoolDetailViewControllable {
            schoolDetailViewModel?.visibleSchool = school
            let viewModel: SchoolDetailViewModelable = schoolDetailViewModel ?? SchoolDetailViewModel(school: school)
            detailController.viewModel = viewModel
            schoolDetailViewModel = viewModel
            let detailCoordinator = SchoolDetailCoordinator(selectedSchool: school,
                                                            viewController: detailController,
                                                            navigationController: navigationController)
            coordinators.append(detailCoordinator)
            detailCoordinator.start()
        }
    }
}
