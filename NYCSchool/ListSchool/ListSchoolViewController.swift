//
//  ListSchoolViewController.swift
//  NYCSchool
//
//  Created by Rémy Virin on 19/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

import UIKit

protocol ListSchoolCoordinatable: NavigationCoordinatable {
    var controller: ListSchoolViewControllable { get }
    func showDetailFor(school: Schoolable)
}

final class ListSchoolViewController: UIViewController, ListSchoolViewControllable {
    
    @IBOutlet weak var tableView: UITableView!
    var viewModel: ListSchoolViewModelable!
    weak var listener: ListSchoolCoordinatable?

    private let cellIdentifier = "defaultIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Schools in NYC"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
    }
    
    func displaySchoolList() {
        viewModel.fetchSchools { [weak self] (schools, error) in
            guard error == nil else {
                // TODO: Handle network error
                return
            }
            self?.tableView.reloadData()
        }
    }
}

extension ListSchoolViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        if indexPath.row < viewModel.schools.count {
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.text = viewModel.schools[indexPath.row].name
        }
        
        return cell
    }
}

extension ListSchoolViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row < viewModel.schools.count {
            listener?.showDetailFor(school: viewModel.schools[indexPath.row])
        }
    }
}
