//
//  ListSchoolViewModel.swift
//  NYCSchool
//
//  Created by Rémy Virin on 19/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

import Foundation

protocol ListSchoolViewModelable: ViewModelable {
    var schools: [Schoolable] { get }
    func fetchSchools(callback: @escaping ([Schoolable]?, Error?) -> ())
}

final class ListSchoolViewModel: ListSchoolViewModelable {
    
    private let network = Network()
    
    var schools = [Schoolable]()
    
    func fetchSchools(callback: @escaping ([Schoolable]?, Error?) -> ()) {
        network.executeRequest(api: .schoolList) { (json, error) in
            guard error == nil else { return callback(nil, error) }
        
            if let array = json as? [[String:Any]] {
                let schools = array.compactMap { School(dict: $0) }
                self.schools = schools
                callback(schools, nil)
            }
        }
    }
}
