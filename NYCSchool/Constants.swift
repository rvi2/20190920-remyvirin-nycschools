//
//  Constants.swift
//  NYCSchool
//
//  Created by Rémy Virin on 19/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

struct UIConstants {
    static let mainStoryboard = "Main"
    static let listSchoolViewController = "ListSchoolViewControllerID"
    static let schoolDetailViewController = "SchoolDetailViewControllerID"
    static let SatView = "SatView"
}
