//
//  AppCoordinator.swift
//  NYCSchool
//
//  Created by Rémy Virin on 19/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

import UIKit


final class AppCoordinator: Coordinatable {
    
    var coordinators = [Coordinatable]()
    
    private let navigationController: UINavigationController
    private let window: UIWindow?
    private let listSchoolCoordinator: ListSchoolCoordinatable?
    
    init(window: UIWindow?) {
        self.window = window
        let storyboard = UIStoryboard(name: UIConstants.mainStoryboard, bundle: nil)
        if let controller = storyboard.instantiateViewController(withIdentifier: UIConstants.listSchoolViewController) as? ListSchoolViewControllable {
            navigationController = UINavigationController(rootViewController: controller)
            listSchoolCoordinator = ListSchoolCoordinator(controller: controller,
                                                          navigationController: navigationController)
        } else {
            assertionFailure("Couldn't allocate listSchoolViewController from storyboard: \(storyboard)")
            listSchoolCoordinator = nil
            navigationController = UINavigationController()
        }
    }
    
    func start() {
        guard let window = window,
            let listSchoolCoordinator = listSchoolCoordinator else { return }
        coordinators.append(listSchoolCoordinator)
        listSchoolCoordinator.start()
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
    }
    
    func finish() {}
}
