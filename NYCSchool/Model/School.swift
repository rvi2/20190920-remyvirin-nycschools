//
//  School.swift
//  NYCSchool
//
//  Created by Rémy Virin on 19/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

import Foundation

protocol Schoolable: Modable {
    var dbn: String { get }
    var name: String { get }
    
    var satMath: String? { get }
    var satWriting: String? { get }
    var satReading: String? { get }
    
}

struct School: Schoolable {
    let dbn: String
    let name: String
    let satMath: String?
    let satWriting: String?
    let satReading: String?
    
    init?(dict: [String: Any]) {
        guard let dbn = dict["dbn"] as? String,
            let name = dict["school_name"] as? String
            else { return nil }
        
        self.dbn = dbn
        self.name = name
        
        self.satMath = dict["sat_math_avg_score"] as? String
        self.satWriting = dict["sat_writing_avg_score"] as? String
        self.satReading = dict["sat_critical_reading_avg_score"] as? String
    }
}
