//
//  UIViewExtensions.swift
//  NYCSchool
//
//  Created by Rémy Virin on 20/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

import UIKit

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
