//
//  SchoolDetailViewController.swift
//  NYCSchool
//
//  Created by Rémy Virin on 20/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

import UIKit

protocol SchoolDetailCoordinatable: Coordinatable {
    var viewController: SchoolDetailViewControllable { get }
}

protocol SchoolDetailViewModelable: ViewModelable {
    var satScore: [(label: String, value: String)] { get }
    var visibleSchool: Schoolable { get set }
    func retrieveSAT(callback: @escaping (_ school: Schoolable)->()) throws
}

final class SchoolDetailViewController: UIViewController, SchoolDetailViewControllable {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var noSATScoreLabel: UILabel!
    
    var viewModel: SchoolDetailViewModelable!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Details"

        do {
            try viewModel.retrieveSAT { [weak self] _ in
                self?.updateUI()
            }
        } catch SchoolDetailError.SATNotaAvailable {
            label.text = viewModel.visibleSchool.name
            noSATScoreLabel.isHidden = false
        } catch {
            assertionFailure("Error should be catched")
        }
        

    }
    
    private func updateUI() {
        label.text = viewModel.visibleSchool.name
        stackView.arrangedSubviews.forEach { stackView.removeArrangedSubview($0) }
        viewModel.satScore.forEach { (label, value) in
            let satView: SATView = .fromNib()
            satView.label.text = label
            satView.valueLabel.text = value
            stackView.addArrangedSubview(satView)
        }
    }
}

