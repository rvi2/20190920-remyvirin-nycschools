//
//  SchoolDetailCoordinator.swift
//  NYCSchool
//
//  Created by Rémy Virin on 20/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

import UIKit

protocol SchoolDetailViewControllable: ViewControllable {
    var viewModel: SchoolDetailViewModelable! { get set }
}


final class SchoolDetailCoordinator: NavigationCoordinatable {
    
    let viewController: SchoolDetailViewControllable
    let navigationController: UINavigationController
    var coordinators = [Coordinatable]()
    
    init(selectedSchool: Schoolable,
         viewController: SchoolDetailViewControllable,
         navigationController: UINavigationController) {
        
        self.viewController = viewController
        self.navigationController = navigationController
    }
    
    func start() {
        navigationController.pushViewController(viewController, animated: true)
    }
}
