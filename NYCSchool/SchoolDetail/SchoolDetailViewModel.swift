//
//  SchoolDetailViewModel.swift
//  NYCSchool
//
//  Created by Rémy Virin on 20/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

import Foundation

enum SchoolDetailError: Error {
    case SATNotaAvailable
}

final class SchoolDetailViewModel: SchoolDetailViewModelable {
    
    private let network = Network()
    var schoolsWithSAT: [Schoolable]?
    var visibleSchool: Schoolable

    var satScore: [(label: String, value: String)] {
        return [("Math: ",visibleSchool.satMath),
                ("Reading: ",visibleSchool.satReading),
                ("Writing: ",visibleSchool.satWriting)]
            // if SAT score is nil don't return it and it's label
            .compactMap { ($0.1 == nil) ? nil : ($0.0, ($0.1 ?? "")) }
    }
    
    init(school: Schoolable) {
        visibleSchool = school
    }

    func retrieveSAT(callback: @escaping (_ school: Schoolable) ->()) throws {
        if let schoolsSAT = schoolsWithSAT {
            if let school = schoolsSAT.first(where: { $0.dbn == visibleSchool.dbn }) {
                visibleSchool = school
                callback(school)
            } else {
                throw SchoolDetailError.SATNotaAvailable
            }
        } else {
            fetchSchoolsSAT { [weak self] (schools, error) in
                // TODO: handle error
                guard let sSelf = self else { return }
                sSelf.schoolsWithSAT = schools
                if let school = schools?.first(where: { $0.dbn == sSelf.visibleSchool.dbn }) {
                    sSelf.visibleSchool = school
                    callback(sSelf.visibleSchool)
                } else {
                    // Throw error if not SAT found
                    //throw SchoolDetailError.SATNotaAvailable
                }
            }
        }
    }
    
     private func fetchSchoolsSAT(callback: @escaping (_ schools: [Schoolable]?, _ error: Error?) -> ()) {
        
        network.executeRequest(api: .schoolSAT) { [weak self] (json, error) in
            guard let sSelf = self else { return }
            if let array = json as? [[String:Any]] {
                let schools = array.compactMap { School(dict: $0) }
                sSelf.schoolsWithSAT = schools
                callback(schools, nil)
            }
        }
    }
}
