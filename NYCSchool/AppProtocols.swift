//
//  AppProtocols.swift
//  NYCSchool
//
//  Created by Rémy Virin on 19/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

import UIKit

protocol Coordinatable: class {
    var coordinators: [Coordinatable] { get }
    
    func start()
    func finish()
}

protocol NavigationCoordinatable: Coordinatable {
    var navigationController: UINavigationController { get }
}

extension Coordinatable {
    func finish() {
        coordinators.forEach { $0.finish() }
    }
}

protocol ViewModelable {}
protocol Modable {}

protocol ViewControllable: UIViewController {}
