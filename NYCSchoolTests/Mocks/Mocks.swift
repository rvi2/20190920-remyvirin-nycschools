//
//  File.swift
//  NYCSchoolTests
//
//  Created by Rémy Virin on 20/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

import UIKit
@testable import NYCSchool

class ListSchoolViewControllableMock: UIViewController, ListSchoolViewControllable {
    var viewModel: ListSchoolViewModelable!
    var listener: ListSchoolCoordinatable?
    
    var displaySchoolListCallCount = 0
    func displaySchoolList() {
        displaySchoolListCallCount += 1
    }    
}
