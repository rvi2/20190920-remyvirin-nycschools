//
//  SchoolTests.swift
//  NYCSchoolTests
//
//  Created by Rémy Virin on 20/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

import XCTest
@testable import NYCSchool


class SchoolTests: XCTestCase {

    var dict: [String : Any] = ["dbn": "XYZ",
                                "school_name": "Manhattan School",
                                "sat_math_avg_score": "323",
                                "sat_writing_avg_score": "333",
                                "sat_critical_reading_avg_score": "444"]
    
    func test_init_Withoutdbn_nil() {
        dict.removeValue(forKey: "dbn")
        let school = School(dict: dict)
        XCTAssertNil(school)
    }
    
    func test_init_WithoutSchoolName_nil() {
        dict.removeValue(forKey: "school_name")
        let school = School(dict: dict)
        XCTAssertNil(school)
    }
    
    func test_init_withoutSAT_correct() {
        dict.removeValue(forKey: "sat_math_avg_score")
        dict.removeValue(forKey: "sat_writing_avg_score")
        dict.removeValue(forKey: "sat_critical_reading_avg_score")
       
        let school = School(dict: dict)
        
        XCTAssertNotNil(school)
        XCTAssertEqual(school?.dbn, "XYZ")
        XCTAssertEqual(school?.name, "Manhattan School")
        XCTAssertNil(school?.satMath)
        XCTAssertNil(school?.satWriting)
        XCTAssertNil(school?.satReading)
    }
    
    
    
    func test_init_correctDict_correct() {
        let school = School(dict: dict)

        XCTAssertNotNil(school)
        XCTAssertEqual(school?.dbn, "XYZ")
        XCTAssertEqual(school?.name, "Manhattan School")
        XCTAssertEqual(school?.satMath, "323")
        XCTAssertEqual(school?.satWriting, "333")
        XCTAssertEqual(school?.satReading, "444")
    }
    
    

}
