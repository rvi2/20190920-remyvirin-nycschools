//
//  LIstSchoolCoordinatorTests.swift
//  NYCSchoolTests
//
//  Created by Rémy Virin on 20/09/2019.
//  Copyright © 2019 RemyVirin. All rights reserved.
//

import XCTest
@testable import NYCSchool

class ListSchoolCoordinatorTests: XCTestCase {

    let controller = ListSchoolViewControllableMock()
    lazy var coordinator = ListSchoolCoordinator(controller: controller, navigationController: UINavigationController())
    
    
    func test_init_controller_andItsListener_set() {
        XCTAssertEqual(controller, coordinator.controller)
        XCTAssert(controller.listener === coordinator)
    }
    
    func testStart_callControllerDisplaySchoolList() {
        XCTAssertEqual(controller.displaySchoolListCallCount, 0)
        coordinator.start()
        XCTAssertEqual(controller.displaySchoolListCallCount, 1)
    }
}
