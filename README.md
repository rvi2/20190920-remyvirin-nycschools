
NYC School
==========

_Remy Virin_

At first, I wanted to use [RIBS](https://github.com/Uber/ribs), as I'm familiar with it and it's a great mobile architecture. But after few thoughts, I decide to take this opportunity to learn a new design architecture, I used MVVMC, and love it! 
I didn't add a lot of tests, just a few to give an idea on how I will test the code.

There is a couple of things I would do differently in a _real project_:
- An improved version of MVVMC, for instance I chose to do everything with protocols (Coordinatable, ViewModable, ViewControllable etc...) I believe this is a correct approach for mocking purposes but having base classes could help as well, for the coordinators for instance
- Have a database to store the data, specially for offline purposes 
- Using a dependency to generate mocks, and add more tests obviously
- Not sure about Storyboards and XIB, it really helps for a small project like this, but with a good autolayout DSL like [Snapkit](http://snapkit.io/) for instance, writing view code is not that time consuming and can ease development with multiple developers.

